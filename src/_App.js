import {Redirect, Route} from "react-router-dom";
import React from "react";
import {HydraAdmin} from "@api-platform/admin";
import authProvider from "./authProvider";
import {
    ResourceGuesser,
    ListGuesser,
    ShowGuesser,
    CreateGuesser,
    EditGuesser,
    FieldGuesser,
    InputGuesser,
    hydraDataProvider as baseHydraDataProvider,
    fetchHydra as baseFetchHydra,
    useIntrospection,
} from "@api-platform/admin";
import parseHydraDocumentation from "@api-platform/api-doc-parser/lib/hydra/parseHydraDocumentation";
import { fetchUtils } from 'react-admin';
import simpleRestProvider from 'ra-data-simple-rest';


const httpClient = (url, options = {}) => {
    if (!options.headers) {
        options.headers = new Headers({ Accept: 'application/json' });
    }
    const { token } = JSON.parse(localStorage.getItem('auth'));
    options.headers.set('Authorization', 'Basic YWRtaW46YWRtaW4=');
    return fetchUtils.fetchJson(url, options);
};
const dataProvider = simpleRestProvider('https://127.0.0.1:8000/api', httpClient);
// Replace with your own API entrypoint
// For instance if https://example.com/api/books is the path to the collection of book resources, then the entrypoint is https://example.com/api
export default () => (

    <HydraAdmin entrypoint="https://127.0.0.1:8000/api"

                authProvider={authProvider}/>
);
