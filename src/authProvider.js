export default {
    login: ({ username, password }) => {
        const request = new Request(
            'https://127.0.0.1:8000/api',
            {
                method: "POST",
                body: JSON.stringify({ email: username, password }),
                headers: new Headers({ "Content-Type": "application/json", "Authorization": "Basic " + btoa(username + ":" + password) }),
            }
        );
        return fetch(request)
            .then((response) => {
                if (response.status < 200 || response.status >= 300) {
                    throw new Error(response.statusText);
                }
                return "Basic " + btoa(username + ":" + password) ;
            })
            .then(({ token }) => {
                localStorage.setItem("token", token);
            });
    },
    logout: () => {
        localStorage.removeItem("token");
        return Promise.resolve();
    },
    checkAuth: () => {
        try {
            if (
                !localStorage.getItem("token")
            ) {
                return Promise.reject();
            }
            return Promise.resolve();
        } catch (e) {
            // override possible jwtDecode error
            return Promise.reject();
        }
    },
    checkError: (err) => {
        if ([401, 403].includes(err?.status || err?.response?.status)) {
            localStorage.removeItem("token");
            //return Promise.reject();
            return Promise.reject();
        }
        return Promise.resolve();
    },
    getPermissions: () => Promise.resolve(),
};
